#ifndef FRUTAS_H
#define FRUTAS_H 

#include "produtos.h"

class Frutas : public Produto{
	private:
		int lote;
		int data;

	public:
		Frutas *next;
		Frutas *prev;
		Frutas();
		Frutas(int c, string d, float p, int q, int l, int dt);
		~Frutas();	
		void set_lote(int l);
		int get_lote();
		void set_data(int dt);
		int get_data();

		void set_cdb(int c);
		int get_cdb();
		void set_desc(string d);
		string get_desc();
		void set_preco(float p);
		float get_preco();
		void set_quant(int q);
		int get_quant();

		
};


#endif