#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include "bebidas.h"
#include "frutas.h"

class Lista {
	private:
		int quant;

	public:
		Bebidas *bh;
		Bebidas *bt;
		Frutas	*fh;
		Frutas	*ft;
		Lista();
		~Lista();
		int get_quant();
		void inserir_inicio_b(int c, string d, float p, int q, float t, float a);
		void inserir_inicio_f(int c, string d, float p, int q, int l, int da);
		void listar();
};



#endif