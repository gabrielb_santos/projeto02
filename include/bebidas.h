#ifndef BEBIDAS_H
#define BEBIDAS_H

#include "produtos.h"

class Bebidas : public Produto{
	private:
		float teor;
		float acucar;
	

	public:
		Bebidas *next;
		Bebidas *prev;
		Bebidas();
		Bebidas(int c, string d, float p, int q, float t, float a);
		~Bebidas();
		void set_teor(float t);
		float get_teor();
		void set_acucar(float a);
		float get_acucar();

		void set_cdb(int c);
		int get_cdb();
		void set_desc(string d);
		string get_desc();
		void set_preco(float p);
		float get_preco();
		void set_quant(int q);
		int get_quant();

};

#endif
