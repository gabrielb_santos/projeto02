PROG = ./bin/Qlevetudo

INC_DIR = ./include 
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc

CC = g++

RM = rm -rf 

OBJS = ./build/main.o ./build/lista.o ./build/bebidas.o ./build/frutas.o

CPPFLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)


.PHONY: init all programa01  programa02 programa03 doxy doc clean go

debug: CFLAGS += -g -O0


$(PROG): $(OBJ_DIR) $(OBJS)
	$(CC) $(OBJS) $(CPPFLAGS) -o $@

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp
	$(CC) -c $(CPPFLAGS) $^ -o $@

$(OBJ_DIR)/lista.o: $(SRC_DIR)/lista.cpp 
	$(CC) -c $(CPPFLAGS) $^ -o $@

$(OBJ_DIR)/bebidas.o: $(SRC_DIR)/bebidas.cpp 
	$(CC) -c $(CPPFLAGS) $^ -o $@

$(OBJ_DIR)/frutas.o: $(SRC_DIR)/frutas.cpp 
	$(CC) -c $(CPPFLAGS) $^ -o $@


$(OBJ_DIR):
	mkdir $@


doxy:
	doxygen Doxyfile

doc:
	$(RM) $(DOC_DIR)/*

clean:
	$(RM) $(OBJ_DIR)/*
	$(RM) $(BIN_DIR)/*

go:
	$(BIN_DIR)/Qlevetudo