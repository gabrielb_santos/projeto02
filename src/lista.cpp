#include "lista.h"

Lista::Lista() {
	bh = new Bebidas();
	bt = new Bebidas();

	bh -> next = bt;
	bt -> prev = bh;
	bh -> prev = NULL;
	bt -> next = NULL;

	fh = new Frutas();
	ft = new Frutas();

	fh -> next = ft;
	ft -> prev = fh;
	fh -> prev = NULL;
	ft -> next = NULL;
}

Lista::~Lista(){

}

//int get_quant();

void Lista::inserir_inicio_b(int c, string d, float p, int q, float t, float a){
	Bebidas *n = new Bebidas(c, d, p, q, t, a);
	Bebidas *h = bh;
	
	h->next->prev = n;
	n->next = h->next;
	h->next = n;
	n->prev = h;
}


void Lista::inserir_inicio_f(int c, string d, float p, int q, int l, int da){
	Frutas *n = new Frutas(c, d, p, q, l, da);
	Frutas *h = fh;
	
	h->next->prev = n;
	n->next = h->next;
	h->next = n;
	n->prev = h;
}

void Lista::listar(){
	Bebidas *h = bh->next;
	cout << "\n-----Setor de Bebidas-----\n";
	for (; h != bt ; h = h->next)
	{
		cout << "\nDescrição: " << h->get_desc() << "\nCódigo de barras: " << h->get_cdb();
		cout << "\nPreço: " << h->get_preco() << "\nQuantidade: " << h->get_quant();
		cout << "\nTeor alcólico: " << h->get_teor() << "\nQuantidade de açúcar por ml: " << h->get_acucar() << endl;
	}

	Frutas *f = fh->next;
	cout << "\n-----Setor de Frutas-----\n";
	for (; f != ft ; f = f->next)
	{
		cout << "\nDescrição: " << f->get_desc() << "\nCódigo de barras: " << f->get_cdb();
		cout << "\nPreço: " << f->get_preco() << "\nQuantidade: " << f->get_quant();
		cout << "\nNúmero do lote: " << f->get_lote() << "\nData do lote: " << f->get_data() << endl;
	}
}