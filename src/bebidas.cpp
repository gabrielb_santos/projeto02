#include "bebidas.h"

Bebidas::Bebidas(){
	teor = 0;
	acucar = 0;
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
}

Bebidas::Bebidas(int c, string d, float p, int q, float t, float a){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	set_teor(t);
	set_acucar(a);
	next = NULL;
	prev = NULL;
}


void Bebidas::set_teor(float t){
	teor = t;
}

float Bebidas::get_teor(){
	return teor;
}

void Bebidas::set_acucar(float a){
	acucar = a;
}

float Bebidas::get_acucar(){
	return acucar;
}

void Bebidas::set_cdb(int c){
	cdb = c;
}

int Bebidas::get_cdb(){
	return cdb;
}

void Bebidas::set_desc(string d){
	desc = d;
}

string Bebidas::get_desc(){
	return desc;
}

void Bebidas::set_preco(float p){
	preco = p;
}

float Bebidas::get_preco(){
	return preco;
}

void Bebidas::set_quant(int q){
	quant = q;
}

int Bebidas::get_quant(){
	return quant;
}