#include "frutas.h"

Frutas::Frutas(){
	lote = 0;
	data = 0;
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
}

Frutas::Frutas(int c, string d, float p, int q, int l, int dt){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	set_lote(l);
	set_data(dt);
}

Frutas::~Frutas(){

}

void Frutas::set_lote(int l){
	lote = l;
}

int Frutas::get_lote(){
	return lote;
}

void Frutas::set_data(int dt){
	data = dt;
}

int Frutas::get_data(){
	return data;
}

void Frutas::set_cdb(int c){
	cdb = c;
}

int Frutas::get_cdb(){
	return cdb;
}

void Frutas::set_desc(string d){
	desc = d;
}

string Frutas::get_desc(){
	return desc;
}

void Frutas::set_preco(float p){
	preco = p;
}

float Frutas::get_preco(){
	return preco;
}

void Frutas::set_quant(int q){
	quant = q;
}

int Frutas::get_quant(){
	return quant;
}