#include "produtos.h"

void Produtos::set_cdb(int c){
	cdb = c;
}

int Produtos::get_cdb(){
	return cdb;
}

void Produtos::set_desc(string d){
	desc = d;
}

string Produtos::get_desc(){
	return desc;
}

void Produtos::set_preco(float p){
	preco = p;
}

float Produtos::get_preco(){
	return preco;
}

void Produtos::set_quant(int q){
	quant = q;
}

int Produtos::get_quant(){
	return quant;
}