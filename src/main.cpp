#include <iostream>
#include <string>
#include "lista.h"
#include <limits>
#include <stdlib.h>

using namespace std;

void ClearScreen(){
    cout << "\033[2J\033[1;1H"; // codigo retirado da stackoverflow http://stackoverflow.com/questions/4062045/clearing-terminal-in-linux-with-c-code
}


void invalida(int &num){
	while (!(cin >> num)) {
    cin.clear(); 
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    cout << "Entrada inválida. Por favor, digite um valor válido.\n";
	}
}


void invalida(float &num){
	while (!(cin >> num)) {
    cin.clear(); 
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    cout << "Entrada inválida. Por favor, digite um valor válido.\n";
	}
}


void cadastrar(Lista &l){
	ClearScreen();

	cout << "Qual tipo de produto deseja cadastrar:\n";
	cout << "\n1 - Bebidas";
	cout << "\n2 - Frutas\n";

	int escolha;
	do{
		invalida(escolha);
	}while(escolha > 2 || escolha < 1);

	cout << "\nCódigo de barras (6 digitos): ";
	float codigo;
	invalida(codigo);

	cout << "\nDescrição: ";
	string desc;
	cin.ignore();
	getline(cin, desc, '\n');

	cout << "\nPreço: ";
	float preco;
	invalida(preco);

	cout << "\nQuantidade: ";
	int qnt;
	invalida(qnt);

	if(escolha == 1){
		cout << "\nTeor alcólico: ";
		float teor;
		invalida(teor);

		cout << "\nQuantidade de açúcar por ml: ";
		float acucar;
		invalida(acucar);

		l.inserir_inicio_b(codigo, desc, preco, qnt, teor, acucar);
	}else if(escolha == 2){
		cout << "\nNúmero do lote: ";
		int lote;
		invalida(lote);

		cout << "\nData do lote: ";
		int data;
		invalida(data);

		l.inserir_inicio_f(codigo, desc, preco, qnt, lote, data);
	}
}


void acessar_lista(Lista &l){

	int escolha;
	ClearScreen();

	inicio:
	cout << "\n---------Gerenciador de cadastros de produtos---------\n";
	cout << "\n1 - Cadastar novo produto";
	cout << "\n2 - Lista de produtos cadastrados";
	cout << "\n3 - Sair\n\n";


	escolha = 0;
	do{	
		invalida(escolha);
		
	}while(escolha > 3 || escolha < 1);

	ClearScreen();
	switch(escolha){
		case 1:
			cadastrar(l);
			goto inicio;
			break;

		case 2:
			l.listar();
			goto inicio;
			break;

		case 3:
			break;

		default:
			break;

	}
}

int main() {
	Lista l;

	acessar_lista(l);

	return 0;
}