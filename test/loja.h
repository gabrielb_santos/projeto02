#ifndef LOJA_H
#define LOJA_H

#include <string>

#include "produtos.h"

class Loja {
	private:
		int q_estoque;
		float arrecadado;
		//Fornecedor *f;

	public:
		Loja();
		~Loja();
		void cadastro_p(int tipo, int quant, float preco);
		void remocao_p(int tipo, int quant);
		void consulta_p();
		void consulta_f();
		void cadastro_f();
		//void venda();
		void get_estoque();
	
};

#endif